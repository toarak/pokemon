# Assignment04Pokemon

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 12.2.4.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.

## ABOUT
This is a short Pokemon storage programm. Just Login with your name, then see which Pokemon you've already catched. If you want to catch more just go to the Pokedex and start catching them all
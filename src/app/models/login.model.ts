export class Login {
    constructor(public name: string, public catched: string[]) {}
 
    static create(event: {name: string, catched: string[]}) {
     return {name: event.name, catched: event.catched}
    };


    public getName() {
        return this.name;
    }

    public getCatched() {
        return this.catched
    }

    public setName(name: string) {
        this.name = name;
    }

    public setCatched(catched: string) {
        this.catched.push(catched);
    }

}

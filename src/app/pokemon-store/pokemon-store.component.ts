import { Component, OnInit } from '@angular/core';
import { PokemonService } from '../services/pokemon.service';
import { LoginsService } from '../services/login.service';
import { Login } from '../models/login.model';

@Component({
  selector: 'app-pokemon-store',
  templateUrl: './pokemon-store.component.html',
  styleUrls: ['./pokemon-store.component.css']
})
export class PokemonStoreComponent implements OnInit {

  pokemons: any[] = [];
  page: number = 1;
  totalPokemons: number = 0;
  trainer = new Login("",[]);
 


  constructor(private pokemonService: PokemonService, private loginsService: LoginsService) { }

  ngOnInit(): void {
    this.getStoredPokemons();
  }

  getStoredPokemons() {

    //get the pokemons
    this.pokemonService.getAllPokemons()
    .subscribe((response: any) => {
      response.results.forEach((result: { name: string; }) => {

        //give me the data for the trainer
        this.trainer = this.loginsService.trainer;

        let catched: string[] = this.trainer.getCatched();
        
        catched.forEach(catched => {

          if (result.name == catched)
          //get Data per Pokemon
          this.pokemonService.getPokemonData(result.name)
          .subscribe((pokeResponse: any) => {
            this.pokemons.push(pokeResponse);
          });
          
        });
 

      });
            
    });
  }

}


import { Component, OnInit } from '@angular/core'
import { NgForm } from '@angular/forms';
import { Login } from '../models/login.model';
import { LoginsService } from '../services/login.service'

@Component({
    selector: 'app-login',
    templateUrl: './logins.component.html',
    styleUrls: ['./logins.component.css']
})

export class LoginsComponent implements OnInit{

    public trainer = new Login("",[])
 
    constructor(private readonly loginService: LoginsService) {
    }

    //Init the function we want to use
    ngOnInit(): void {
        this.loginService.fetchLogins();
    }

    get logins(): Login {
        return this.loginService.logins()
    }

    public onLoginPush(createForm: NgForm) : void {

        var found: boolean = false;
        this.trainer.setName(createForm.value)

        //check if login is already existing
        if(this.logins.name === this.trainer.getName()){
            //here comes the routing when done
            found = true;
            return;
        }      

        //if not create one
        if(found == false){         
            this. loginService.newLogin(this.trainer);
        }

        //save in LocalStorage
        localStorage.setItem("trainer", this.trainer.getName())

            
    }

    

}
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginPage } from './login-page/login-page.page';
import { PokemonListComponent } from './pokemon-list/pokemon-list.component';
import { PokemonStoreComponent } from './pokemon-store/pokemon-store.component';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: '/login-page'
  },
  {
    path: 'login-page',
    component: LoginPage
  },
  {
    path: 'pokedex',
    component: PokemonListComponent
  },
  {
    path: 'store',
    component: PokemonStoreComponent
  }
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule { }

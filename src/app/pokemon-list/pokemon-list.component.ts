import { Component, OnInit } from '@angular/core';
import { PokemonService } from '../services/pokemon.service';
import { LoginsService } from '../services/login.service';

@Component({
  selector: 'app-pokemon-list',
  templateUrl: './pokemon-list.component.html',
  styleUrls: ['./pokemon-list.component.css']
})
export class PokemonListComponent implements OnInit {

  pokemons: any[] = [];
  page: number = 1;
  totalPokemons: number = 0;

  constructor(private pokemonService: PokemonService, private loginsService: LoginsService) { }

  ngOnInit(): void {
    this.getPokemons();
  }

  getPokemons() {
      //get the pokemons
      this.pokemonService.getPokemons(10, this.page + 0)
      .subscribe((response: any) => {
        //set total counter
        this.totalPokemons = response.count;
  
        response.results.forEach((result: { name: string; }) => {
  
          //get Data per Pokemon
          this.pokemonService.getPokemonData(result.name)
            .subscribe((pokeResponse: any) => {
                this.pokemons.push(pokeResponse);

            });
              
          });
      });
  }

  //what happens if you click the button
  onCellButtonClick(pokemonName: string){
    this.loginsService.trainer.setCatched(pokemonName);
  }

}

//Module Section
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgxPaginationModule } from 'ngx-pagination';

//App Section
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

//Component Section
import { LoginsComponent } from './login/logins.component';
import { HeaderComponent } from './header/header.component';
import { PokemonListComponent } from './pokemon-list/pokemon-list.component';
import { PokemonStoreComponent } from './pokemon-store/pokemon-store.component';

//Page Section
import { LoginPage } from './login-page/login-page.page';
import { LoginsService } from './services/login.service';

//Service Section


@NgModule({ //Decorator
  declarations: [
    //list of components
    AppComponent,
    LoginsComponent,
    LoginPage,
    HeaderComponent,
    PokemonListComponent,
    PokemonStoreComponent
  ],
  imports: [
    //list of modules
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    BrowserAnimationsModule,
    NgxPaginationModule
  ],
  providers: [LoginsService],
  bootstrap: [AppComponent]
})

export class AppModule { }

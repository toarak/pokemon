import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Login } from '../models/login.model';

@Injectable({
    providedIn: 'any'
})

export class LoginsService {

    public trainer = new Login("",[])
    private _error: string = '';

    //Dependency Injection
    constructor(private readonly http: HttpClient) {
    }

    //fetch existing logins from JSON DB
    public fetchLogins(): void {
        //need to define type of request
        this.http.get<Login>('http://localhost:3000/logins')
        .subscribe( (logins: Login) => {
            this.trainer = logins;
        },
        (error: HttpErrorResponse) => {
            this._error = error.message;
        })
    }

    //create new Login when needed
    public newLogin( newTrainer: Login): void {
        this.http.post('http://localhost:3000/logins', newTrainer);
    }

    //create a getter
    public logins(): Login {
        return this.trainer;
    }

    public erros(): string {
        return this._error;
    }

    //special getter
    public getLoginByName(trainerName: any) : Login {
        this.http.get<Login>('http://localhost:3000/logins')
        .subscribe((logins: Login) => {
                
           this.trainer = logins

        },
        (error: HttpErrorResponse) => {
            this._error = error.message;
            
        })

        return this.trainer;
    }

}
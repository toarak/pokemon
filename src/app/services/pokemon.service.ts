import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";

@Injectable({
    providedIn: 'root'
})

export class PokemonService {

    constructor( private http: HttpClient) {}
    
    //get all Pokemons from API
    getAllPokemons(){
        return this.http.get('https://pokeapi.co/api/v2/pokemon')
    }
    
    //get Pokemons from API per side
    getPokemons(limit: number, offset: number){
        return this.http.get('https://pokeapi.co/api/v2/pokemon?limit='+limit+'&offset='+offset)
    }

    //details per pokemon
    getPokemonData(name: string){
        return this.http.get('https://pokeapi.co/api/v2/pokemon/'+name)

    }
    

}